exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema
      .createTable("paymententity", function(table) {
        table.string("id");
        table.string("payment_id");
        table.string("entity");
        table.integer("amount");
        table.string("currency");
        table.string("status");
        table.string("method");
        table.string("order_id");
        table.string("invoice_id");
        table.string("card_id");
        table.string("bank");
        table.string("wallet");
        table.string("vpa");
        table.string("description");
        table.boolean("international");
        table.integer("amount_refunded");
        table.string("refund_status");
        table.boolean("captured");
        table.string("email");
        table.string("contact");
        table.json("notes");
        table.integer("fee");
        table.integer("tax");
        table.string("error_code");
        table.string("error_description");
        table.timestamp("created_at");
        table.timestamp("updated_at");
      })
      .createTable("payoutentity", function(table) {
        table.string("id");
        table.string("account_number");
        table.integer("fund_account_id");
        table.string("currency");
        table.string("amount");
        table.string("mode");
        table.string("purpose");
        table.string("reference_id");
        table.string("narration");
        table.json("notes");
        table.timestamp("created_at");
        table.timestamp("updated_at");
        table.integer("fees");
        table.integer("tax");
        table.string("status");
        table.string("utr");
        table.string("batch_id");
        table.string("failure_reason");
      })
      .createTable("refundentity", function(table) {
        table.string("id");
        table.string("entity");
        table.integer("amount");
        table.string("currency");
        table.string("payment_id");
        table.timestamp("created_at");
        table.json("notes");
        table.timestamp("updated_at");
      })
      .createTable("orderentity", function(table) {
        table.string("id");
        table.string("entity");
        table.integer("amount");
        table.string("currency");
        table.string("receipt");
        table.string("status");
        table.integer("attempts");
        table.json("notes");
        table.timestamp("created_at");
        table.timestamp("updated_at");
      })

      .createTable("settlemententity", function(table) {
        table.string("id");
        table.string("entity");
        table.integer("amount");
        table.integer("tax");
        table.integer("fees");
        table.string("status");
        table.string("utr");
        table.timestamp("created_at");
        table.timestamp("updated_at");
      })

      .createTable("gigstatransactioninfo", function(table) {
        table.increments("id").primary();
        table.integer("sender_id");
        table.integer("sender_role_id");
        table.integer("appointment_id");
        table.integer("receiver_id");
        table.integer("receiver_role_id");
        table.integer("amount");
        table.integer("customer_id");
        table.integer("account_id");
        table.string("settlement_id");
        table.string("settlement_status");
        table.string("paymentid");
        table.string("refundid");
        table.string("orderid");
        table
          .integer("transaction_type")
          .references("gigstatransactiontypes.code");
        table
          .integer("transaction_status")
          .references("gigstatransactionstatuses.code");
        table.timestamp("created_at");
        table.timestamp("updated_at");
      }),

    knex.schema
      .createTable("gigstatransactionstatuses", function(table) {
        table.increments("id").primary();
        table
          .integer("code")
          .unique()
          .notNullable();
        table
          .string("name")
          .unique()
          .notNullable();
      })
      .then(function() {
        return knex("gigstatransactionstatuses").insert([
          { code: "1", name: "CLIENT_PAYMENT_INITIATED" },
          { code: "2", name: "THIRD_PARTY_PAYMENT_INITIATED" },
          { code: "3", name: "THIRD_PARTY_PAYMENT_AUTHORIZED" },
          { code: "4", name: "THIRD_PARTY_PAYMENT_CAPTURED" },
          { code: "5", name: "THIRD_PARTY_PAYMENT_SETTLED" }
        ]);
      }),

    knex.schema
      .createTable("gigstatransactiontypes", function(table) {
        table.increments("id").primary();
        table
          .integer("code")
          .unique()
          .notNullable();
        table
          .string("name")
          .unique()
          .notNullable();
      })
      .then(function() {
        return knex("gigstatransactiontypes").insert([
          { code: "1", name: "TASK_COMPLETION_PAYMENT" },
          { code: "2", name: "CREDITS_BOUGHT" },
          { code: "3", name: "TASK_ADD_PAYMENT" }
        ]);
      })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("paymententity"),
    knex.schema.dropTableIfExists("refundentity"),
    knex.schema.dropTableIfExists("orderentity"),
    knex.schema.dropTableIfExists("settlemententity"),
    knex.schema.dropTableIfExists("gigstatransactioninfo"),
    knex.schema.dropTableIfExists("gigstatransactionstatuses"),
    knex.schema.dropTableIfExists("gigstatransactiontypes")
  ]);
};
