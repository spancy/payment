var { QueryBuilder } = require("objection");
var Gigstatransactioninfo = require("../models/Gigstatransactioninfo");
var Gigstatransactionstatuses = require("../models/Gigstatransactionstatuses");
var Gigstatransactiontypes = require("../models/Gigstatransactiontypes");
var Orderentity = require("../models/Orderentity");
var Paymententity = require("../models/Paymententity");
var Refundentity = require("../models/Refundentity");
var Settlemententity = require("../models/Settlemententity");
const axios = require("axios");
const config=require('../config');
//function to Update into template table and relations.
async function paymentUpdate(req) {
  try {
    let message = "";
    const payment = await Paymententity.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated Paymententity Details",
        id: payment.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated Paymententity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function payoutUpdate(req) {
  try {
    let message = "";
    const payout = await Payoutentity.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated Payoutentity Details",
        id: payout.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated Payoutentity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function orderUpdate(req) {
  try {
    let message = "";
    const order = await Orderentity.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Orderentity Updated",
        id: order.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update Orderentity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function refundentityUpdate(req) {
  try {
    let message = "";
    const refundentity = await Refundentity.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated refundentity ",
        id: refundentity.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated refundentityErrored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function settlemententityUpdate(req) {
  try {
    let message = "";
    const settlemententity = await Settlemententity.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated settlemententity",
        id: settlemententity.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated settlemententity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function gigstatransactiontypesUpdate(req) {
  try {
    let message = "";
    const gigstatransactiontypes = await Gigstatransactiontypes.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated Gigstatransactiontypes",
        id: gigstatransactiontypes.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated Gigstatransactiontypes Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function gigstatransactionstatusesUpdate(req) {
  try {
    let message = "";
    const gigstatransactionstatuses = await Gigstatransactionstatuses.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated Gigstatransactionstatuses ",
        id: gigstatransactionstatuses.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated Gigstatransactionstatuses Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function gigstatransactioninfoUpdate(req) {
  try {
    let message = "";
    const gigstatransactioninfo = await Gigstatransactioninfo.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated Gigstatransactioninfo ",
        id: gigstatransactioninfo.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated Gigstatransactioninfo Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

module.exports.paymentUpdate = paymentUpdate;
module.exports.payoutUpdate = payoutUpdate;
module.exports.refundentityUpdate = refundentityUpdate;
module.exports.orderUpdate = orderUpdate;
module.exports.settlemententityUpdate = settlemententityUpdate;
module.exports.gigstatransactiontypesUpdate = gigstatransactiontypesUpdate;
module.exports.gigstatransactionstatusesUpdate = gigstatransactionstatusesUpdate;
module.exports.gigstatransactioninfoUpdate = gigstatransactioninfoUpdate;
