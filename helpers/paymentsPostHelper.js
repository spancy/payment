var { QueryBuilder } = require("objection");
var Gigstatransactioninfo = require("../models/Gigstatransactioninfo");
var Gigstatransactionstatuses = require("../models/Gigstatransactionstatuses");
var Gigstatransactiontypes = require("../models/Gigstatransactiontypes");
var Orderentity = require("../models/Orderentity");
var Paymententity = require("../models/Paymententity");
var Refundentity = require("../models/Refundentity");
var Settlemententity = require("../models/Settlemententity");
const config=require('../config');
const axios = require("axios");
const Razorpay = require("razorpay");
async function paymentInsert(req) {
  const paymentData = {
    id: req.body.payment_id,
    entity: req.body.entity,
    amount: req.body.amount,
    currency: req.body.currency,
    status: req.body.status,
    method: req.body.method,
    order_id: req.body.order_id,
    invoice_id: req.body.invoice_id,
    card_id: req.body.card_id,
    bank: req.body.bank,
    wallet: req.body.wallet,
    vpa: req.body.vpa,
    description: req.body.description,
    international: req.body.international,
    amount_refunded: req.body.amount_refunded,
    refund_status: req.body.refund_status,
    captured: req.body.captured,
    email: req.body.email,
    contact: req.body.contact,
    notes: req.body.notes,
    fee: req.body.fee,
    tax: req.body.tax,
    error_code: req.body.error_code,
    error_description: req.body.error_description,
    created_at: req.body.created_at
  };

  try {
    let message = "";
    const payment = await Paymententity.query().insertGraph(paymentData);

    message = {
      statusCode: 200,
      body: {
        message: "New Paymententity Created",
        id: payment.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Paymententity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function payoutInsert(req) {
  const payoutData = {
    account_number: req.body.account_number,
    amount: req.body.amount,
    currency: req.body.currency,
    fund_account_id: req.body.fund_account_id,
    mode: req.body.mode,
    order_id: req.body.order_id,
    purpose: req.body.purpose,
    reference_id: req.body.reference_id,
    narration: req.body.narration,
    notes: req.body.notes,
    payout_id: req.body.payout_id,
    fees: req.body.fees,
    tax: req.body.tax,
    status: req.body.status,
    utr: req.body.utr,
    batch_id: req.body.batch_id,
    failure_reason: req.body.failure_reason,
    created_at_rp: req.body.created_at_rp
  };

  try {
    let message = "";
    const payout = await Payoutentity.query().insertGraph(payoutData);

    message = {
      statusCode: 200,
      body: {
        message: "New Payout entity Created",
        id: payout.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Payoutentity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Razorpay Payment API
async function razorpayPayment(req) {
  const axios = require("axios");
  const config=require('../config');
  const key=config.key;
  const secret=config.secret;
  const id=req.body.razorpay_payment_id;
  const amount = req.body.amount;
  let message ='';
  console.log("Id", id);
  console.log("amount", amount);

  const payload = {
    amount: amount,
    currency: "INR"
  };

  try {
    var options = {
      method:'POST',
      url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/payments/'+id+'/capture',
      data: payload
    };
    console.log(options);
    const response = await axios(options);
   console.log("Response is "+ response.data);
    return response.data;
  } catch (err) {
    console.log("Error Response is ",  {status:err.response.status,statusText:err.response.statusText});
    return {status:err.response.status,statusText:err.response.statusText};
  }
/*  let os = await (instance.payments.capture(id,amount,
    (error, response) => {
    if (error) {
      console.log("Capture",error.error.code + error.error.description);
      message={
        code:error.error.code,
        description:error.error.description
      }
    } else {
      console.log("Capture",response);
      message=response;
      return response;
    }
  }))*/
  //return message.statusCode;

//return message;

}

//End of payment API

//Razorpay Payout API call
async function razorpayPayout(req) {
  const axios = require("axios");
  const config=require('../config');
  const key=config.key;
  const secret=config.secret;
console.log("Logggg",key);
console.log("Logggg",secret);
console.log("Logggg",config.acc_number);
  const payload = {
    account_number: config.acc_number,
    amount: req.body.amount,
    currency: req.body.currency,
    fund_account_id: req.body.fund_account_id,
    mode: req.body.mode,
    order_id: req.body.order_id,
    purpose: req.body.purpose,
    reference_id: req.body.reference_id,
    narration: req.body.narration,
    notes: req.body.notes,
    payout_id: req.body.payout_id,
    fees: req.body.fees,
    tax: req.body.tax,
    status: req.body.status,
    utr: req.body.utr,
    batch_id: req.body.batch_id,
    failure_reason: req.body.failure_reason,
    created_at_rp: req.body.created_at_rp
  };


  try {
    var options = {
      method:'POST',
      url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/payouts',
      data: payload
    };
    //console.log(options);
    const response = await axios(options);
   console.log("Response is "+ response.data);
    return response.data;
  } catch (err) {
    console.log("Error Response is ",  {status:err.response.status,statusText:err.response.statusText});
    return {status:err.response.status,statusText:err.response.statusText};
  }
}

//End of Razorpay Payout call
async function refundInsert(req) {
  const refundData = {
    entity: req.body.entity,
    amount: req.body.amount,
    currency: req.body.currency,
    payment_id: req.body.payment_id,
    notes: req.body.notes,
    created_at: req.body.created_at
  };

  try {
    let message = "";
    const refund = await Refundentity.query().insertGraph(refundData);

    message = {
      statusCode: 200,
      body: {
        message: "New Refundentity Created",
        id: refund.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Refundentity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function refundRazorpayInsert(req) {
  const refundData = {
    payment_id: req.body.payment_id
  };

  try {
    var options = {
      method:'POST',
      url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/payments/'+refundData.payment_id+'refund',
      data: payload
    };
    //console.log(options);
    const response = await axios(options);
   console.log("Response is "+ response.data);
    return response.data;
  } catch (err) {
    return err;
  }
}


async function orderInsert(req) {
  const orderData = {
    entity: req.body.entity,
    amount: req.body.amount,
    currency: req.body.currency,
    status: req.body.status,
    receipt: req.body.receipt,
    attempts: req.body.attempts,
    notes: req.body.notes,
    created_at: req.body.created_at
  };

  try {
    let message = "";
    const order = await Orderentity.query().insertGraph(orderData);

    message = {
      statusCode: 200,
      body: {
        message: "New Orderentity Created",
        id: order.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Orderentity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function settlementInsert(req) {
  const settlementData = {
    entity: req.body.entity,
    amount: req.body.amount,
    tax: req.body.tax,
    status: req.body.status,
    fees: req.body.fees,
    utr: req.body.utr,
    created_at: req.body.created_at
  };

  try {
    let message = "";
    const settlement = await Settlemententity.query().insertGraph(
      settlementData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New Settlemententity Created",
        id: settlement.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Settlemententity Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}
async function gigstatransactiontypesInsert(req) {
  const gigstatransactiontypesData = {
    code: req.body.code,
    name: req.body.name
  };

  try {
    let message = "";
    const gigstatransactiontypes = await Gigstatransactiontypes.query().insertGraph(
      gigstatransactiontypesData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New Gigstatransactiontype Created",
        id: gigstatransactiontypes.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Gigstatransactiontype Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function gigstatransactionstatusesInsert(req) {
  const gigstatransactionstatusesData = {
    code: req.body.code,
    name: req.body.name
  };

  try {
    let message = "";
    const gigstatransactionstatuses = await Gigstatransactionstatuses.query().insertGraph(
      gigstatransactionstatusesData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New Gigstatransactionstatus Created",
        id: gigstatransactionstatuses.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Gigstatransactionstatus Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function gigstatransactioninfoInsert(req) {
  const gigstatransactioninfoData = {
    sender_id: req.body.sender_id,
    sender_role_id: req.body.sender_role_id,
    receiver_id: req.body.receiver_id,
    receiver_role_id: req.body.receiver_role_id,
    amount: req.body.amount,
    customer_id: req.body.customer_id,
    account_id: req.body.account_id,
    settlement_id: req.body.settlement_id,
    settlement_status: req.body.settlement_status,
    paymentid: req.body.paymentid,
    refundid: req.body.refundid,
    orderid: req.body.orderid,
    transaction_type: req.body.transaction_type,
    transaction_status: req.body.transaction_status,
    created_at: req.body.created_at,
    appointment:req.body.appointment
  };

  try {
    let message = "";
    const gigstatransactioninfo = await Gigstatransactioninfo.query().insertGraph(
      gigstatransactioninfoData
    );

    message = {
      statusCode: 200,
      body: {
        message: "New Gigstatransactioninfo Created",
        id: gigstatransactioninfo.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Gigstatransactioninfo Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

module.exports.paymentInsert = paymentInsert;
module.exports.razorpayPayment = razorpayPayment;
module.exports.razorpayPayout = razorpayPayout;
module.exports.payoutInsert = payoutInsert;
module.exports.refundInsert = refundInsert;
module.exports.orderInsert = orderInsert;
module.exports.settlementInsert = settlementInsert;
module.exports.gigstatransactiontypesInsert = gigstatransactiontypesInsert;
module.exports.gigstatransactionstatusesInsert = gigstatransactionstatusesInsert;
module.exports.gigstatransactioninfoInsert = gigstatransactioninfoInsert;
module.exports.refundRazorpayInsert = refundRazorpayInsert;
