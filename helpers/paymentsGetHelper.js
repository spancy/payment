var { QueryBuilder } = require("objection");
var Gigstatransactioninfo = require("../models/Gigstatransactioninfo");
var Gigstatransactionstatuses = require("../models/Gigstatransactionstatuses");
var Gigstatransactiontypes = require("../models/Gigstatransactiontypes");
var Orderentity = require("../models/Orderentity");
var Paymententity = require("../models/Paymententity");
var Refundentity = require("../models/Refundentity");
var Settlemententity = require("../models/Settlemententity");
const axios = require("axios");
const config=require('../config');
//function to get all templates and its underlying relations
async function gigstatransactioninfoGetData(req) {
  try {
    const gigstatransactioninfoData = await Gigstatransactioninfo.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: gigstatransactioninfoData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function gigstatransactioninfoGetDataByTaskID(req,queryBy) {
  try {
    const gigstatransactioninfoData = await Gigstatransactioninfo.query().where(
      queryBy,
      req.params[queryBy]
    );

    let message = "";

    message = {
      statusCode: 200,
      body: gigstatransactioninfoData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function gigstatransactioninfoGetDataBySenderID(req) {
  try {
    const gigstatransactioninfoData = await Gigstatransactioninfo.query().where(
      queryBy,
      req.params.sender_id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: gigstatransactioninfoData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function gigstatransactionstatusesGetData(req, queryBy) {
  try {
    const gigstatransactionstatusesData = await Gigstatransactionstatuses.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: gigstatransactionstatusesData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function gigstatransactiontypesGetData(req, queryBy) {
  try {
    const gigstatransactiontypesData = await Gigstatransactiontypes.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: gigstatransactiontypesData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function orderentityGetData(req, queryBy) {
  try {
    const orderentityData = await Orderentity.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: orderentityData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function paymententityGetData(req, queryBy) {
  try {
    const paymententityData = await Paymententity.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: paymententityData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Razorpay payments GET call
async function getrazorpayPayment(req) {
  const axios = require("axios");
  const config=require('../config');
  const key=config.key;
  const secret=config.secret;
  const id=req.params.id;


  try {
    var options = {
      method:'GET',
      url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/payments/'+id,
      data: ""
    };
    //console.log(options);
    const response = await axios(options);
   console.log("Response is "+ response.data);
    return response.data;
  } catch (err) {
    return err;
  }
}

async function payoutentityGetData(req, queryBy) {
  try {
    const payoutentityData = await Payoutentity.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: payoutentityData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function refundentityGetData(req, queryBy) {
  try {
    const refundentityData = await Refundentity.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: refundentityData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function settlemententityGetData(req, queryBy) {
  try {
    const settlemententityData = await Settlemententity.query().where(
      queryBy,
      req.params.id
    );

    let message = "";

    message = {
      statusCode: 200,
      body: settlemententityData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

module.exports.gigstatransactioninfoGetData = gigstatransactioninfoGetData;
module.exports.gigstatransactionstatusesGetData = gigstatransactionstatusesGetData;
module.exports.orderentityGetData = orderentityGetData;
module.exports.paymententityGetData = paymententityGetData;
module.exports.getrazorpayPayment = getrazorpayPayment;
module.exports.payoutentityGetData = payoutentityGetData;
module.exports.refundentityGetData = refundentityGetData;
module.exports.settlemententityGetData = settlemententityGetData;
module.exports.gigstatransactioninfoGetDataBySenderID = gigstatransactioninfoGetDataBySenderID;
module.exports.gigstatransactiontypesGetData = gigstatransactiontypesGetData;
module.exports.gigstatransactioninfoGetDataByTaskID = gigstatransactioninfoGetDataByTaskID;
