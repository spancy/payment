const bodyParser = require("body-parser");

const cookieParser = require("cookie-parser");

let app = require("express")();


let gigstatransactioninfo = require("./controllers/gigstatransactioninfo");
let orders = require("./controllers/orders");
let payments = require("./controllers/payments");
let paymentscontroller = require("./controllers/paymentscontroller");
let payouts = require("./controllers/payouts");
let refunds = require("./controllers/refunds");
let settlements = require("./controllers/settlements");


const { Model } = require("objection");
const knex = require("knex");

const KnexConfig = require("./knexfile");

Model.knex(knex(KnexConfig.development));
//body and cookie parsers

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// Add headers
app.use(function(req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

app.all("/", function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

//Initialize routing dependencies

//app.use("/task/search", taskSearch);
app.use("/payments/gigstatransactioninfo/", gigstatransactioninfo);
app.use("/payments/orders/", orders);
app.use("/payments/payments/", payments);
app.use("/payments/paymentscontroller/", paymentscontroller);
app.use("/payments/payouts/", payouts);
app.use("/payments/refunds/", refunds);
app.use("/payments/settlements/", settlements);


//app.use("/task/instance", instance);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err);
  res.send("error");
});

/*
app.listen(process.env.port||"7000");
console.log("listening on port 7000")
*/
module.exports = app;
