var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/paymentsPostHelper");
var getHelper = require("../helpers/paymentsGetHelper");
var putHelper = require("../helpers/paymentsPutHelper");

const knex = require("knex");

router.route("/").post(async function(req, res) {
  let response = await postHelper.payoutInsert(req);
  res.json(response);
});

router.route("/razorpay/payout").post(async function(req, res) {
  let response = await postHelper.razorpayPayout(req);
  res.json(response);
});

router.route("/:id").put(async function(req, res) {
  let response = await putHelper.payoutUpdate(req);
  res.json(response);
});

router.route("/getByID/:id").get(async function(req, res) {
  let response = await getHelper.payoutentityGetData(req, "id");
  res.json(response);
});
/*router
  .route("/getByEmployerID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.employerGetAddressData(req, "employer_id");
    res.json(response);
  });*/

module.exports = router;
