var express = require('express');
var router = express.Router();
const config=require('../config');


function getRazorPayUrl(key,secret,id){
return 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/payments/'+id;
}


function postRazorPayUrl(key,secret,id){
return 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/payments/'+id+'/capture';
}

function performRESTCall(url,method,body){


  var url = url;
  var options ={
    method:method,
      uri : url
    };
    if(method=='POST')
    options ={
      method:method,
        uri : url,
        body:body,
         json: true
      };
 return new Promise(function(resolve, reject) {
  request( options, function(error, response, body) {
      console.log(error);
      if(error)
      reject(error);
      else {
        resolve(body);
      }

    } );
  });
  /*

  var options = {
    host : host,
        path:  path,
        port:8080

         };
  if(method==='POST')
  options = {
    host : host,
        path:  path,
           method: method,
             body:body

         };
  console.log(options);
   return new Promise(function(resolve, reject) {
   http.get(options, function (http_res) {
      // initialize the container for our data
      var data = "";

      // this event fires many times, each time collecting another piece of the response
      http_res.on("data", function (chunk) {
          // append this chunk to our growing `data` var
          data += chunk;
      });

      // this event fires *one* time, after all the `data` events/chunks have been gathered
      http_res.on("end", function () {
          // you can use res.send instead of console.log to output via express
          console.log(data);
            resolve(data);

      });

      http_res.on("error", function (e) {
          // you can use res.send instead of console.log to output via express
          console.log(e);
            reject(e);

      });
    });


      });

*/
}





router.route('/thirdparty/:razorpay_payment_id')
.get(function (req, res) {

const key=config.key;
const secret=config.secret;
const id=req.params.razorpay_payment_id;

const url=getRazorPayUrl(key,secret,id);
console.log(url);
var restCall=performRESTCall(url,'GET','');
restCall
.then(function (data) {
//var data=performRESTCall(url,'GET','');
console.log(data);
  res.json({status:200,error: false, data: JSON.parse(data)});
}, function(err) {
        console.log(err);
        res.json({status:500,error: true,data:err});
    })
.catch(function (err) {
  res.status(500).json({error: true, data: {message: err.message}});
})
})

.put(function (req, res) {

const key=config.key;
const secret=config.secret;
const id=req.params.razorpay_payment_id;

const url=postRazorPayUrl(key,secret,id);
console.log(url);
console.log(req.body);

var restCall=performRESTCall(url,'POST',req.body);
restCall
.then(function (data) {
//var data=performRESTCall(url,'GET','');
PaymentEntity.forge({
  id: data.payment_id,
  entity: data.entity,
  amount: data.amount,
  currency: data.currency,
  status: data.status,
  method: data.method,
  order_id: data.order_id,
  invoice_id: data.invoice_id,
  card_id: data.card_id,
  bank: data.bank,
  wallet: data.wallet,
  vpa: data.vpa,
  description: data.description,
  international: data.international,
  amount_refunded: data.amount_refunded,
  refund_status: data.refund_status,
  captured: data.captured,
  email: data.email,
  contact: data.contact,
  notes: data.notes,
  fee: data.fee,
  tax: data.tax,
  error_code: data.error_code,
  error_description: data.error_description,
  created_at: data.created_at
})
.save()
.then(function (payments) {
  res.json({error: false, data: data});
})
.catch(function (err) {
  res.status(500).json({error: true, data: {message: err.message}});
});

console.log(data);

}, function(err) {
        console.log(err);
        res.json({status:500,error: true,data:err});
    })
.catch(function (err) {
  res.status(500).json({error: true, data: {message: err.message}});
})
})


// create an address



module.exports = router;
