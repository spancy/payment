var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/paymentsPostHelper");
var getHelper = require("../helpers/paymentsGetHelper");
var putHelper = require("../helpers/paymentsPutHelper");

const knex = require("knex");

router
  .route("/")
  .post(async function(req, res) {
    let response = await postHelper.paymentInsert(req);
    res.json(response);
  });


  router
    .route("/razorpayPost")
    .post(async function(req, res) {
      let response = await postHelper.razorpayPayment(req);
      res.json(response);
    });
/*router
  .route("/:id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.paymentUpdate(req);
    res.json(response);
  });*/

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.paymententityGetData(req, "id");
    res.json(response);
  });

  router.route("/getByRazorpayID/:id").get(async function(req, res) {
    let response = await getHelper.getrazorpayPayment(req, "id");
    res.json(response);
  });

  router
    .route("/:id")
    .put(async function(req, res) {
      let response = await putHelper.paymentUpdate(req);
      res.json(response);
    });
/*router
  .route("/getByEmployerID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.employerGetAddressData(req, "employer_id");
    res.json(response);
  });*/

module.exports = router;
