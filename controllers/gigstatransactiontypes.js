var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/paymentsPostHelper");
var getHelper = require("../helpers/paymentsGetHelper");
var putHelper = require("../helpers/paymentsPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.gigstatransactiontypesInsert(req);
    res.json(response);
  });

router
  .route("/:id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.gigstatransactiontypesUpdate(req);
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.gigstatransactiontypesGetData(req, "id");
    res.json(response);
  });
/*router
  .route("/getByEmployerID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.employerGetAddressData(req, "employer_id");
    res.json(response);
  });*/

module.exports = router;
