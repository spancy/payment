"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class Gigstatransactionstatuses extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "gigstatransactionstatuses";
  }
}
module.exports = Gigstatransactionstatuses;
