"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class Gigstatransactioninfo extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "gigstatransactioninfo";
  }

  static get relationMappings() {
    return {
      gigstatransactionstatus: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Gigstatransactionstatuses",
        join: {
          from: "gigstatransactioninfo.transaction_status",
          to: "gigstatransactionstatuses.code"
        }
      },
      gigstatransactiontype: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Gigstatransactiontypes",
        join: {
          from: "gigstatransactioninfo.transaction_type",
          to: "gigstatransactiontypes.code"
        }
      }
    };
  }
}
module.exports = Gigstatransactioninfo;
