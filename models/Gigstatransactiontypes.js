"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class Gigstatransactiontypes extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "gigstatransactiontypes";
  }
}
module.exports = Gigstatransactiontypes;
